<?php
namespace App\Model;
use PDO;

//require_once '../../../../config/init.php';

class Book_Author extends Database {
    
    public function insert_new_book_author($book_id, $auth_id) {
        $sql  = "INSERT INTO books_authors ";
        $sql .= "(book_id, author_id) VALUES ";
        $sql .= "(:book_id, :auth_id) ";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':book_id', $book_id, PDO::PARAM_INT);
        $stmt->bindParam(':auth_id', $auth_id, PDO::PARAM_INT);
        $stmt->execute();
        
        if ($this->checkError($stmt)) {
            return true;
        }
        
        return false;
    }
    
}