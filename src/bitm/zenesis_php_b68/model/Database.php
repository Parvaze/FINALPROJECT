<?php
namespace App\Model;
use PDO, PDOException;

//require_once '../../../../config/db_config.php';

class Database {
    
    public $connection;
    
    public function __construct() {
        global $host, $db_name, $port, $user_name, $password;
        
        try {
            $this->connection = new PDO("mysql:host=$host;dbname=$db_name;port=$port", $user_name, $password);
        } catch (PDOException $ex) {
            trigger_error($ex->getMessage(), E_USER_ERROR);
        }
    }
    
    public function checkError($conn) {
        $error_info = $conn->errorInfo();
        
        if ($error_info[2]) {
            trigger_error($error_info[2], E_USER_ERROR);
        } else {
            return true;
        }
    }
}