<?php
namespace App\Model;
use PDO;

//require_once '../../../config/init.php';

class Category extends Database {
    
    public function get_all_categories() {
        $sql = "SELECT * FROM categories";

        $result = $this->connection->query($sql);
        if ($result->rowCount()) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    public function get_category_by_id($id) {
        $sql  = "SELECT * FROM categories ";
        $sql .= "WHERE id = :id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':id' => $id]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function get_category_by_name($cat_name) {
        $sql  = "SELECT * FROM categories ";
        $sql .= "WHERE name = :name LIMIT 1";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':name' => $cat_name]);

        if ($this->checkError($stmt)) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        }

        return false;
    }
    
}