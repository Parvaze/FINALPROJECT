<?php
namespace App\Model;
use PDO;

//require_once '../../../../config/init.php';

class Book extends Database {

    public $title;

    public function get_books_by_title($title) {
        $this->title = "%$title%";
        $sql = "SELECT * FROM books WHERE title LIKE :title";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['title' => $this->title]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        return false;
    }

    public function get_all_books() {
        $sql = "SELECT * FROM books";

        $result = $this->connection->query($sql);
        if ($result->rowCount()) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    public function get_books_by_category($cat_id) {
        $sql  = "SELECT * FROM categories LEFT JOIN books ";
        $sql .= "ON categories.id = books.category_id ";
        $sql .= "WHERE categories.id = :category_id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':category_id' => $cat_id]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function get_books_by_author($auth_id) {
        $sql  = "SELECT * FROM authors LEFT JOIN books_authors ";
        $sql .= "ON authors.id = books_authors.author_id ";
        $sql .= "LEFT JOIN books ";
        $sql .= "ON books.id = books_authors.book_id ";
        $sql .= "WHERE authors.id = :auth_id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['auth_id' => $auth_id]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function get_book_info($book_id) {
        $sql  = "SELECT books.id as book_id, ";
        $sql .= "books.title as book_title, ";
        $sql .= "categories.id as cat_id, ";
        $sql .= "categories.name as cat_name, ";
        $sql .= "books.publication as publication, ";
        $sql .= "books.price as price, ";
        $sql .= "books.cover_img as cover_img, ";
        $sql .= "authors.name as author_name ";
        $sql .= "FROM books LEFT JOIN categories ";
        $sql .= "ON books.category_id = categories.id ";
        $sql .= "LEFT JOIN books_authors ";
        $sql .= "ON books.id = books_authors.book_id ";
        $sql .= "LEFT JOIN authors ";
        $sql .= "ON authors.id = books_authors.author_id ";
        $sql .= "WHERE books.id = :book_id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':book_id' => $book_id]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function insert_book($title, $cat_id, $publication, $price, $cover_img) {
        $sql  = "INSERT INTO books ";
        $sql .= "(title, category_id, publication, price, cover_img) VALUES ";
        $sql .= "(:title, :cat_id, :publication, :price, :cover_img)";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':cat_id', $cat_id, PDO::PARAM_INT);
        $stmt->bindParam(':publication', $publication);
        $stmt->bindParam(':price', $price, PDO::PARAM_INT);
        $stmt->bindParam(':cover_img', $cover_img, PDO::PARAM_STR);
        
        $stmt->execute();
         
        if ($this->checkError($stmt)) {
            return $this->connection->lastInsertId();
        }
        
        return false;
    }
    
    public function update_book($id, $title, $cat_id, $publication, $price, $cover_img) {
        $sql  = "UPDATE books SET ";
        $sql .= "title = :title, ";
        $sql .= "category_id = :cat_id, ";
        $sql .= "publication = :publication ";
        $sql .= "price = :price, ";
        $sql .= "cover_img = :cover_img, ";
    }

}

