<?php
namespace App\Model;
use PDO;

//require_once '../../../../config/init.php';

class Author extends Database {
    
    public function get_author_by_name($name) {
        $sql = "SELECT * FROM authors WHERE name = :name LIMIT 1";

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['name' => $name]);
        
   
        if ($this->checkError($stmt) && $stmt->rowCount()) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function get_all_authors() {
        $sql = "SELECT * FROM authors";
        
        $result = $this->connection->query($sql);
        if ($result->rowCount()) {
            return $result->fetchAll(PDO::FETCH_OBJ);
        }
    }
    
    public function get_authors_by_book_title($book_title) {
        $sql  = "SELECT * FROM books LEFT JOIN books_authors ";
        $sql .= "ON books.id = books_authors.book_id ";
        $sql .= "LEFT JOIN authors ";
        $sql .= "ON authors.id = books_writers.author_id ";
        $sql .= "WHERE books.title LIKE :book_title";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':book_title' => "%$book_title%"]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function get_authors_by_book_id($book_id) {
        $sql = "SELECT * FROM books LEFT JOIN books_authors ";
        $sql .= "ON books.id = books_authors.book_id ";
        $sql .= "LEFT JOIN authors ";
        $sql .= "ON authors.id = books_authors.author_id ";
        $sql .= "WHERE books.id = :book_id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':book_id' => $book_id]);

        if ($this->checkError($stmt)) {
            return $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        return false;
    }
    
    public function get_author_by_id($id) {
        $sql  = "SELECT * FROM authors ";
        $sql .= "WHERE id = :id";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([':id' => $id]);
        
        if ($this->checkError($stmt)) {
            return $stmt->fetch(PDO::FETCH_OBJ);
        }
        
        return false;
    }
    
    public function insert_new_author($auth_name) {
        $sql  = "INSERT INTO authors (name) VALUES (:name)";
        
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':name', $auth_name);
        $stmt->execute();
 
        if ($this->checkError($stmt)) {
            return $this->connection->lastInsertId();
        }

        return false;
    }

}

//$auth = new Author();
//echo '<pre>';
//var_dump($auth->get_author_by_name('Habi jjabi'));
//echo '</pre>';