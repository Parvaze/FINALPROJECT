<?php
namespace App\Controller;
use App\Controller\ContentController;

class Session {
    
    public function generate_cart_list(array $session_cart_array) {
        $cntrl = new ContentController();
        
        $cart_list = [];
        if (count($session_cart_array)) {
            foreach ($session_cart_array as $cart_item) {
                $cart_list[] = $cntrl->get_book_details($cart_item);
            }
        }
        
        return $cart_list;
    }
            
}
