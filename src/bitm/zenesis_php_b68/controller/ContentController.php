<?php
namespace App\Controller;
use App\Model\Category, App\Model\Book, App\Model\Author, App\Model\Book_Author;

//require_once '../../../../config/init.php';

class ContentController {
    
    public function generate_category_list() {
        $cat = new Category();
        return $cat->get_all_categories();
    }
    
    public function get_category($cat_id) {
        $cat = new Category();
        return $cat->get_category_by_id((int) $cat_id);
    }
    
    public function generate_author_list() {
        $auth = new Author();
        return $auth->get_all_authors();
    }
    
    public function get_author($auth_id) {
        $auth = new Author();
        return $auth->get_author_by_id((int) $auth_id);
    }

    public function get_book_details($book_id) {
        $book_id = (int) $book_id;
        
        $book = new Book();
        $result = $book->get_book_info($book_id);
        
        $count = count($result);
        $authors = '';
        if ($count > 1) {
            for ($i = 0; $i < $count; $i++) {
                $authors .= $result[$i]->author_name . ', ';
            }
            
            $result[0]->author_name = trim($authors, ', ');
        }
        
        return $result[0];
    }
    
    public function generate_book_list_by_category($cat_id) {
        $cat_id = (int) $cat_id;

        $book = new Book();
        $list = $book->get_books_by_category($cat_id);
        
        $book_list = [];
        foreach ($list as $item) {
            if ($item->id) {
                $book_list[] = $this->get_book_details($item->id);
            }
        }

        return $book_list;
    }
    
    public function generate_book_list_for_all_categories() {
        $cat = new Category();
        $book = new Book();
        
        $cat_list = $cat->get_all_categories();
        
        $cat_book_list = [];
        foreach ($cat_list as $category) {
            $list = $book->get_books_by_category($category->id);
            
            $book_list = [];
            foreach($list as $item) {
                if ($item->id) {
                    $book_list[] = $this->get_book_details($item->id);
                }
            }
            
            $cat_book_list[$category->name] = $book_list;
        }
        
        return $cat_book_list;
    }

    public function generate_book_list_by_author($author_id) {
        $author_id = (int) $author_id;
        
        $book = new Book();
        $list = $book->get_books_by_author($author_id);
        
        $book_list = [];
        foreach ($list as $item) {
            if ($item->book_id) {
                $book_list[] = $this->get_book_details($item->book_id);
            }
        }
        
        return $book_list;
    }
    
    public function add_new_book(array $book_info, array $img_file) {
        $cat_id = $auth_id = null;
        
        if (!$this->process_upload($img_file)) {
            return false;
        }
        
        $cat_id = isset($book_info['category']) ? $book_info['category'] : false;
        $book_title = isset($book_info['book-title']) ? $book_info['book-title'] : false;
        $author_name = isset($book_info['author-name']) ? $book_info['author-name'] : false;
        $publication = isset($book_info['publication']) ? $book_info['publication'] : false;
        $price = isset($book_info['price']) ? $book_info['price'] : false;
        $cover_img_name = isset($img_file['name']) ? $img_file['name'] : false;
        
        $auth = new Author();
        $auth_result = $auth->get_author_by_name($author_name);
        
        if (!$auth_result) {
            $auth_id = $auth->insert_new_author($author_name);
        } else {
            $auth_id = $auth_result->id;
        }
        
        $book = new Book();
        $inserted_book_id = $book->insert_book($book_title, $cat_id, $publication, $price, $cover_img_name);
        
        $book_author = new Book_Author();
        if ($book_author->insert_new_book_author($inserted_book_id, $auth_id)) {
            return true;
        }
        
        return false;
    }
    
    public function update_book($book_id, array $book_info, array $img_file) {
        $cat_id = $auth_id = null;

        if (!$this->process_upload($img_file)) {
            return false;
        }
        
        $cat_id = isset($book_info['category']) ? $book_info['category'] : false;
        $book_title = isset($book_info['book-title']) ? $book_info['book-title'] : false;
        $author_name = isset($book_info['author-name']) ? $book_info['author-name'] : false;
        $publication = isset($book_info['publication']) ? $book_info['publication'] : false;
        $price = isset($book_info['price']) ? $book_info['price'] : false;
        $cover_img_name = isset($img_file['name']) ? $img_file['name'] : false;

        $auth = new Author();
        $auth_result = $auth->get_author_by_name($author_name);

        if (!$auth_result) {
            $auth_id = $auth->insert_new_author($author_name);
        } else {
            $auth_id = $auth_result->id;
        }

        $book = new Book();
        $book->update_book($book_id, $book_title, $cat_id, $publication, $price, $cover_img_name);

        $book_author = new Book_Author();
        if ($book_author->insert_new_book_author($inserted_book_id, $auth_id)) {
            return true;
        }

        return false;
    }
    
    public function process_upload(array $img_file) {
        global $ds, $cover_img_root_dir;

        $cover_img_name = isset($img_file['name']) ? $img_file['name'] : false;
        $tmp_location = isset($img_file['tmp_name']) ? $img_file['tmp_name'] : false;

        if (move_uploaded_file($tmp_location, $cover_img_root_dir . $ds . $cover_img_name)) {
            return true;
        }
        
        return false;
    }

}

//$cntrl = new ContentController();
//
//echo '<pre>';
//print_r($cntrl->add_new_book($book_info, $img_file));
//echo '</pre>';
