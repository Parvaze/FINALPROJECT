<?php
session_start();

$ds = DIRECTORY_SEPARATOR;
$root = dirname(__DIR__);
$config_root = __DIR__;

$vendor_root = $root . $ds . 'vendor';
$model_root = $root . $ds . 'src' . $ds . 'bitm' . $ds . 'zenesis_php_b68' . $ds . 'model';
$controller_root = $root . $ds . 'src' . $ds . 'bitm' . $ds . 'zenesis_php_b68' . $ds . 'controller';

$view_root = $root . $ds . 'view' . $ds . 'bitm' . $ds . 'zenesis_php_b68';
$cover_img_root_dir = $view_root . $ds . 'img' . $ds . 'cover_img';

$web_root = '/bitm_b68_php_zenesis_ebook_store/view/bitm/zenesis_php_b68';
$cover_img_root = $web_root . '/img/cover_img';

require_once 'db_config.php';
require_once $vendor_root . $ds . 'autoload.php';