<footer class="well-lg">
    <p>Designed and developed by Zenesis Team. &copy;2017</p>
</footer>

<script src="<?= "{$web_root}/resources/jquery-3.2.1/jquery-3.2.1.min.js"; ?>"></script>
<script src="<?= "{$web_root}/resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"; ?>"></script>
<script src="<?= "{$web_root}/js/main.js"; ?>"></script>
</body>
</html>