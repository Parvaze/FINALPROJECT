<?php 
require_once "../../../config/init.php";
$session = new \App\Controller\Session();

if (!isset($_SESSION['cart-items'])) {
    $_SESSION['cart-items'] = [];
}

(isset($_GET['item-remove'])) && $_GET['item-remove'] === 'all' ? $_SESSION['cart-items'] = [] : null;
        
$item_remove = (isset($_GET['item-remove']) && !empty($_GET['item-remove'])) ? [(int) $_GET['item-remove']] : [];
(empty($item_remove)) ? null : $_SESSION['cart-items'] = array_diff($_SESSION['cart-items'], $item_remove);

$cart_list = $session->generate_cart_list($_SESSION['cart-items']);
$total_price = 0;
?>

<?php include_once './templates/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="clearfix custom-btn-group">
                <a class="btn btn-default" type="submit" href="<?= "{$web_root}/book_list.php" ?>">Go Back</a>
                <a class="btn btn-danger pull-right" href="?item-remove=all">Clear Cart</a>
            </div>
            
            <div class="panel panel-default bg-danger">
                <!-- Default panel contents -->
                <div class="panel-heading">Cart ID: 23452324</div>

                <!-- Table -->
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Book Details</th>
                            <th class="text-center">Price (Taka)</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cart_list as $item): ?>
                        <tr>
                            <td></td>
                            <td><?= "{$item->book_title} by {$item->author_name}"; ?></td>
                            <td class="text-right"><?= "{$item->price}" ?></td>
                            <td class="text-center"><a class="btn btn-danger" href="?item-remove=<?= $item->book_id; ?>">Remove</button></a>
                        </tr>
                        <?php
                            $total_price += $item->price;
                            endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td><?= count($cart_list); ?> books added <span class="pull-right">Total:</span></td>
                            <td class="text-right"><?= $total_price; ?></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Delivery Informations</div>

                <form class="delivery-form">
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" class="form-control" id="first-name" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <label for="middle-name">Middle Name (If any)</label>
                        <input type="text" class="form-control" id="middle-name" placeholder="Middle Name">
                    </div>
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" class="form-control" id="last-name" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <label for="contact-num">Contact Number</label>
                        <input type="text" class="form-control" id="contact-num" placeholder="Contact Number">
                    </div>
                    <div class="form-group">
                        <label for="st-address">Address</label>
                        <input type="text" class="form-control" id="st-address" placeholder="Address">
                    </div>
                    <div class="form-group">
                        <label for="post-office">Post Office</label>
                        <input type="text" class="form-control" id="post-office" placeholder="Post Office">
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" placeholder="City">
                    </div>
                    <div class="form-group">
                        <label for="district">District</label>
                        <input type="text" class="form-control" id="district" placeholder="District">
                    </div>
                    <button type="submit" class="btn btn-success">Checkout</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once './templates/footer.php'; ?>