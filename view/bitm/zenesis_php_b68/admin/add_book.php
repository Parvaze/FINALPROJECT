<?php
require_once '../../../../config/init.php';

$cntrl = new \App\Controller\ContentController();
$cat_list = $cntrl->generate_category_list();

if (isset($_POST) && isset($_FILES['cover-img'])) {
    $cntrl->add_new_book($_POST, $_FILES['cover-img']);
}

?>

<?php include_once '../templates/admin_header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading">Book Informations</div>

                <form class="book-form" action="?" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="sel1">Category</label>
                        <select class="form-control" id="category" name="category">
                            <option selected disabled>Select a category: </option>
                            <?php foreach ($cat_list as $category): ?>
                            <option value="<?= $category->id ?>"><?= $category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="book-title">Book Title</label>
                        <input type="text" class="form-control" name="book-title" id="book-title" placeholder="Book Title">
                    </div>
                    <div class="form-group">
                        <label for="author-name">Author Name</label>
                        <input type="text" class="form-control" name="author-name" id="author-name" placeholder="Author Name">
                    </div>
                    <div class="form-group">
                        <label for="publication">Publication</label>
                        <input type="text" class="form-control" name="publication" id="publication" placeholder="Publication">
                    </div>
                    <div class="form-group">
                        <label for="cover-img">Cover Image</label>
                        <input type="file" class="form-control" name="cover-img" id="cover-img">
                    </div>
                    <div class="form-group">
                        <label for="price">Price (Taka)</label>
                        <input type="number" class="form-control" name="price" id="price" placeholder="Price">
                    </div>

                    <button type="submit" class="btn btn-success">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php include_once '../templates/admin_footer.php'; ?>