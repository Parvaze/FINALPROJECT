<?php 

require_once '../../../../config/init.php';

$cntrl = new \App\Controller\ContentController();
$book_list = $cntrl->generate_book_list_for_all_categories();

?>

<?php include_once '../templates/admin_header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <a class="btn btn-default custom-btn-group" href="add_book.php">Add New Book</a>
            
            <?php foreach ($book_list as $category => $books): ?>
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><?= $category; ?></div>
                <!-- List group -->
                <ul class="list-group">
                    <?php foreach ($books as $book): ?>
                    <li class="list-group-item">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-3">
                                    <img class="thumbnail thumb-img" src="<?= "{$cover_img_root}/{$book->cover_img}"; ?>" alt="<?= $book->book_title; ?>">
                                </div>
                                <div class="col-md-7">
                                    <div>Book Title: <?= $book->book_title; ?></div>
                                    <div>Author: <?= $book->author_name; ?></div>
                                    <div>Publication: <?= $book->publication; ?></div>
                                    <div>Price: <?= $book->price; ?></div>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-primary btn-block" href="edit_book.php?book-id=<?= $book->book_id; ?>">Edit</a>
                                    <a class="btn btn-danger btn-block" href="delete_book.php?book-id=<?= $book->book_id; ?>">Remove</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php include_once '../templates/admin_footer.php'; ?>