<?php
require_once "../../../config/init.php";
(!isset($_GET['cat-id'])) ? header("Location: {$web_root}/book_list.php ") : null;

if (!isset($_SESSION['cart-items'])) {
    $_SESSION['cart-items'] = [];
}

$cntrl = new \App\Controller\ContentController();

$cat_list = $cntrl->generate_category_list();
$auth_list = $cntrl->generate_author_list();

$cur_cat = $cntrl->get_category($_GET['cat-id']);
$cat_book_list = $cntrl->generate_book_list_by_category($_GET['cat-id']);

if (isset($_GET['item-add']) && !empty($_GET['item-add'])) {
    $_SESSION['cart-items'][] = $_GET['item-add'];
}

$item_remove = (isset($_GET['item-remove']) && !empty($_GET['item-remove'])) ? [(int) $_GET['item-remove']] : [];
(empty($item_remove)) ? null : $_SESSION['cart-items'] = array_diff($_SESSION['cart-items'], $item_remove);

?>

<?php include_once './templates/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-md-2">
            <div>
                <div class="h5 text-center text-uppercase">All Categories</div>
                <ul class="nav nav-pills nav-stacked">
                    <?php foreach ($cat_list as $category): ?>
                        <li role="presentation"><a href="<?= "{$web_root}/category_book_list.php?cat-id={$category->id}"; ?>"><?= $category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <div>
                <div class="h5 text-center text-uppercase">All Authors</div>
                <ul class="nav nav-pills nav-stacked">
                    <?php foreach ($auth_list as $author): ?>
                        <li role="presentation"><a href="<?= "{$web_root}/author_book_list.php?auth-id={$author->id}"; ?>"><?= $author->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div> 
        </div>

        <div class="col-md-10 main-content">
            <div class="container-fluid">
                <div class="h5 text-uppercase"><?= $cur_cat->name; ?></div>
                <div class="row">
                    <?php foreach ($cat_book_list as $book_info): ?>
                    <div class="col-md-3">
                        <div class="thumb-item">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <img class="thumb-img" src="<?= "{$cover_img_root}/{$book_info->cover_img}"; ?>" alt="<?= $book_info->book_title; ?>">
                                </li>
                                <li class="list-group-item"><?= $book_info->book_title; ?></li>
                                <li class="list-group-item"><?= $book_info->price; ?></li>
                                <li class="list-group-item"><?= $book_info->author_name; ?></li>
                                <li class="list-group-item"><?= $book_info->publication; ?></li>
                            </ul>
                            
                            <a class="btn btn-primary <?= in_array($book_info->book_id, $_SESSION['cart-items']) ? 'hidden' : ''; ?> item-add" href="?cat-id=<?= $_GET['cat-id']; ?>&item-add=<?= $book_info->book_id; ?>">Add to cart</a>
                            <a class="btn btn-danger <?= in_array($book_info->book_id, $_SESSION['cart-items']) ? '' : 'hidden'; ?> item-remove" href="?cat-id=<?= $_GET['cat-id']; ?>&item-remove=<?= $book_info->book_id; ?>">Remove from cart</a>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                
                <a class="btn btn-default btn-block" href="<?= "{$web_root}/book_list.php"; ?>">Go Back</a>
            </div>
        </div>
    </div>
</div>

<?php include_once './templates/footer.php'; ?>

